const AUTOLIKED_LABEL = 'LMG hit!'; // a string to replace auto-liked "Likes"
const AUTOLIKED_COLOR = 'red'; // autoliked label css color

const clock = 1500; // looking for new items to like each this ms

let LIKE_LABEL = 'Mi piace';
let likePosts = false;
let likeComments = false;
let autoscroll = false;



(function() {

	if (window.hasRun) return;
	window.hasRun = true;

	console.log('Facebook LMG loaded!');


	browser.runtime.onMessage.addListener( message => {

		let _message = message;
		console.log('content.onMessage.message');
		console.log(message);

		if(message.likeLabel !== undefined) LIKE_LABEL = message.likeLabel;
		if(message.likePosts !== undefined) likePosts = message.likePosts;
		if(message.likeComments !== undefined) likeComments = message.likeComments;
		if(message.autoscroll !== undefined) autoscroll = message.autoscroll;

		return Promise.resolve({
			likeLabel: LIKE_LABEL,
			likePosts: (_message.likePosts !== undefined)? _message.likePosts : likePosts,
			likeComments: (_message.likeComments !== undefined)? _message.likeComments : likeComments,
			autoscroll: (_message.autoscroll !== undefined)? _message.autoscroll : autoscroll,
		})

	});


    $j = jQuery.noConflict();


    $j(document).ready(function (){


 		setInterval(function(){

 			console.log('clock');

			// like all posts
			if(likePosts){

				$j( 'span:contains(' + LIKE_LABEL + ')' ).not('.flmg-parsed').each(function( index ) {

					$j(this).addClass('flmg-parsed'); // trick to not parse twice

					if($j(this).html() != '<span>' + LIKE_LABEL + '</span>') return;
					
					if(!$j(this).hasClass('flmg-approved')){

						let _this = $j(this);

						setTimeout(function(){
							// Facebook hate too fast interactions ;)

							$j(_this).addClass('flmg-approved'); // trick to not "unlike"
							$j(_this).html(AUTOLIKED_LABEL).css('color', AUTOLIKED_COLOR);
							$j(_this).trigger('click');
							console.log('post liked');

						}, getRandomInt(33, 333));

					}
					
					
				}); /* span contains */

			}


			// like all comments
			if(likeComments){
				// no needs to track liket comments, aria-label change
				$j('li div[aria-label="' + LIKE_LABEL + '"]').each(function(){

					let _this = $j(this);

					setTimeout(function(){
					    $j(_this).addClass('flmg-approved');
					    $j(_this).html(AUTOLIKED_LABEL).css('color', AUTOLIKED_COLOR);
					    $j(_this).trigger('click');
					    console.log('comment liked');
				    }, getRandomInt(33, 333));

				});

			}


			if(autoscroll) $j("html, body").animate({ scrollTop: $j(document).height()-$j(window).height()});


 		}, clock); /* setInterval */
    	

    }) /* doc ready */


    function getRandomInt(min, max) {
	  min = Math.ceil(min);
	  max = Math.floor(max);
	  return Math.floor(Math.random() * (max - min)) + min; // max excluded, min included
	}
    
    
})();
