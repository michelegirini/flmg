[FACEBOOK] [LMG]
---

##Facebook like machinegun

---

#What

A firefox plugin that automatically likes any post and comment.

Official site: https://flmg.lsdsf.net/
Firefox addon: https://addons.mozilla.org/it/firefox/addon/flmg/

***Be sure to know what you're doing***

---

#Why

We think this way your's profile will be "de-profiled", achieving a more generalized and impartial feed

---

#Initial setup

Edit `flmg.js` to configure the add-on by your's needs:

- `LIKE_LABEL` the string "Like" in your language, for easy multilocale support (**default is in italian**)

- `likePosts` enable/disable posts auto like

- `likeComments` enable/disable comments auto like

- `autoscroll` enable/disable autoscroll feature for full auto fire!

***Remember to hit `reload add-on` if you edit the script while browsing***

*(managing setting from toolbar menu will be available in future releases)*

---

#Loading the plugin

*You'll have to:*

- browse [about:debugging](about:debugging)

- click on "my Firefox" on the left

- click on "load temporary add-on"

- browse and select the `manifest.json` of the project

- a machinegun icon will appear in the toolbar, add-on loaded!

- browse facebook.com

Any auto-liked contents will be highlighted by replacing "Like" with "Mamba Approved"

If get in trouble loading the add-on take a look at the [official Firefox add-on documentation](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing)

---

#When

Start getting de-profiled now!

---

#Who

Ideated by **Frank Monro** [*frankmonro at lsdsf dot net*]

Developed and maintained by **Maicol Gerins** [*maicolgerins at lsdsf dot net*]

---

#Where

- planet Earth, [https://flmg.lsdsf.net](https://flmg.lsdsf.net)