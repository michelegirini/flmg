(function() {
	
	$j = jQuery.noConflict();

	$j(document).ready(function (){

		getStatus();
		
		$j('.options').change(function() {

			setTimeout(function(){
				update();
			}, 50)

	    });


	    $j('#fullAuto').click(function(){

	    	$j('#likePosts').prop('checked', true);
	    	$j('#likeComments').prop('checked', true);
	    	$j('#autoscroll').prop('checked', true);

	    	setTimeout(function(){
				update();
			}, 50)

	    });
		

    }) /* doc ready */


    function getStatus(){

		browser.tabs.query({active: true, currentWindow: true})
	    .then(tabs => {

			browser.tabs.sendMessage(tabs[0].id, {
	          command: 'getStatus',
	        })
	        .then( response => {

	        	console.log('toolbar.getStatus.then.response');
	        	console.log(response);
	        	
	        	if(response.likeLabel) $j('#likeLabel').val(response.likeLabel);
	        	if(response.likePosts) $j('#likePosts').prop('checked', response.likePosts);
	        	if(response.likeComments) $j('#likeComments').prop('checked', response.likeComments);
	        	if(response.autoscroll) $j('#autoscroll').prop('checked', response.autoscroll);

	        });

	    });

	}


	function update(){

	    browser.tabs.query({active: true, currentWindow: true})
	    .then( (tabs) => {

			browser.tabs.sendMessage(tabs[0].id, {
			  likeLabel: $j('#likeLabel').val(),
	          likePosts: $j('#likePosts').is(':checked'),
	          likeComments: $j('#likeComments').is(':checked'),
	          autoscroll: $j('#autoscroll').is(':checked'),
	        })
	        .then( response => {
	        	console.log('toolbar.update.then.response');
	        	console.log(response);
	        })

	    });

	    //.catch();

	}


})();